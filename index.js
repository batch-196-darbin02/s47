console.log("Hello");

// Show Posts Function
const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		console.log(post);
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>

			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries
};

/*
	Fetch Method
	Syntax:
		fetch('URL', options)
		URL - this is the URL which the request is to be made
*/

fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(data => showPosts(data)); // the two (2) .then will handle the manipulation

// Add Post
document.querySelector('#form-add-post').addEventListener("submit", (e) =>{
	//cancelling the event's default action
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value, // input field
			body: document.querySelector("#txt-body").value, // input field
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Post successfully added");

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});

});

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};

// Update Post
document.querySelector(`#form-edit-post`).addEventListener(`submit`, (e) => {

	e.preventDefault();

	// need options since we need to identify the method to be used
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: `PUT`,
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id"),
			title: document.querySelector("#txt-edit-title").value, // input field
			body: document.querySelector("#txt-edit-body").value, // input field
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Post successfully updated");

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector("#btn-submit-update").disabled = true; // or setAttribute('disabled',true)
	})
});

// Delete Post
const deletePost = (id) => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: `DELETE`
	})
	.then(response => response.json())
	.then(response => console.log(response))
	document.querySelector(`#post-${id}`).remove()
}